# Copyright 2022 Christian Gimenez
#
# gmi2html.awk
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {
    codeblock = 0
    antitem = 0
    antblockquote = 0
}

/^```/ {
    if (codeblock) {
        print "</pre>"
        codeblock = 0
    } else {
        match($0, /```[[:space:]]*(.*)/, a)
        print "<pre alt=\"" a[1] "\">"
        codeblock = 1        
    }
    next
}

/./ {
    if (codeblock) {
        print
        next
    }
}

/^>/ {
  if (!antblockquote) {
      print "<quote>"
  }
  antblockquote = 1

  match($0, />[[:space:]]+(.*)$/, a)
  print a[1]
  next
}

/^[^>]/ {
    if (antblockquote) {
        print "</quote>"        
        antblockquote = 0
    }
}

/^\* / {
    if (!antitem) {
        print "<ul>"
    }
    antitem = 1

    match($0, /\*[[:space:]]+(.*)$/, a)
    print "<li>" a[1] "</li>"
    next
}

/^[^\*]/ {
   if (antitem) {
       print "</ul>"
       antitem = 0
   }
}

/^=>/ {
    match($0, /=>[[:space:]]*([^[:space:]]+)([[:space:]]+(.*)$|$)/, matches);
    url=matches[1]
    if (!match(url, /[^:]+:\/\/.*/)){
        # It is a local URl, change gemtext page to HTML page.
        sub(/.gmi$/, ".html", url)
    }
    
    if (matches[2] == "") {
        print "<p><a href=\"" url "\">" matches[1] "</a></p>"
    } else {        
        print "<p><a href=\"" url "\">" matches[2] "</a></p>"
    }
    next
}

/^# / {
    match($0, /#[[:space:]]+(.*)$/, a)
    print "<h1>" a[1] "</h1>"
    next
}

/^## / {
    match($0, /##[[:space:]]+(.*)$/, a)
    print "<h2>" a[1] "</h2>"
    next
}

/^### / {
    match($0, /###[[:space:]]+(.*)$/, a)
    print "<h3>" a[1] "</h3>"
    next    
}

/./ {
    print "<p>" $0 "</p>"
}
