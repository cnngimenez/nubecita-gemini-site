#! /usr/bin/fish

# convert_all_site.fish

# Copyright 2022 cnngimenez

# Author: cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

function gmi2html_clean_site
    for path in (find html/ -name '*.html')
        rm -v  "$path"
    end
end

function gmi2html_gmipath_to_htmlpath \
    --description="Convert \"sitio/GMIPATH\" to \"html/PATH\"."
    set -l filename (basename "$argv[1]" '.gmi')
    set -l dirpath (dirname "$argv[1]")
    set -l dirpath (string sub --start 6 "$dirpath")
    set -l dest "html$dirpath/$filename.html"
    echo "$dest"
end

function gmi2html_convert_gemtext_to_html
    for path in (find sitio/ -name '*.gmi')
        set -l dest (gmi2html_gmipath_to_htmlpath "$path")

        set -l securepath (string sub --start 5 --end 12 "$dest")

        if test "$securepath" != '/secure'
            echo "$path -> $dest"
            mkdir -p (dirname "$dest")
            cat scripts/gmi2html/header_template.html > "$dest"
            gawk -f scripts/gmi2html/gmi2html.awk "$path" >> "$dest"
            cat scripts/gmi2html/footer_template.html >> "$dest"
        else
            set_color brown
            echo "Secure path $path... Ignoring."
            set_color normal
        end
    end
end

function gmi2html_generate_index
    set -l index "$argv[1]/index.html"

    cat scripts/gmi2html/header_template.html > "$index"
    echo "<h1> Index of " (basename "$argv[1]") "</h1>" >> "$index"    
    echo "<ul>" >> "$index"
    for f in (ls -1 "$argv[1]")
        echo "<li><a href=\"$f\">$f</a></li>" >> "$index"
    end
    echo "</ul>" >> "$index"
    cat scripts/gmi2html/footer_template.html >> "$index"
end

function gmi2html_generate_missing_indexes
    set -l directories (find html/ -type d)
    for d in $directories
        if test ! -f "$d/index.html"
            echo "Generating $d/index.html"
            gmi2html_generate_index "$d"
        else
            echo "Not generating $d/index.html"
        end
    end
end

function gmi2html_download_assets
    # Download latex.css. See source: https://github.com/vincentdoerig/latex-css
    # latex.css is under MIT license.
    set -l download_list 'style.min.css' \
        'fonts/LM-bold.woff2' \
        'fonts/LM-bold.woff' \
        'fonts/LM-regular.woff' \
        'fonts/LM-regular.woff2' \
        'fonts/LM-regular.ttf' \
        'fonts/LM-bold.ttf'

    if test ! -d html/css
        mkdir -p 'html/css/latex.css/fonts'
        for f in $download_list
            echo "downloading $f"
            wget -O "html/css/latex.css/$f" "https://unpkg.com/latex.css@1.8.0/$f"
        end
    end

end

set -l curcmd (status current-command)
if test "$curcmd" = "source"
    echo "All gmi2html_* functions loaded"
    exit 0
end

if not test -d sitio
    echo 'Must be in main folder!'
    exit 1
end

echo
set_color --bold
echo "Cleaning html folder..."
set_color normal

gmi2html_clean_site

echo
set_color --bold
echo "Converting Gemtext to HTML..."
set_color normal

gmi2html_convert_gemtext_to_html

echo
set_color --bold
echo "Generating indexs in directories without index.gmi"
set_color normal

gmi2html_generate_missing_indexes

set_color --bold
echo "Download CSS and other assets..."
set_color normal

gmi2html_download_assets

set_color --bold
echo "Done! Have a nice day! 🙂"
set_color normal
