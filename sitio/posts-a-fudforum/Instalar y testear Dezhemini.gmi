# Install Racket 8 in Ubuntu

According to [https://racket-lang.org/download/], there is a PPA repository for Ubuntu. The following commands add the repository to the system and install Racket. 

```
sudo add-apt-repository ppa:plt/racket
sudo apt-get update
sudo apt install racket
```


=> https://racket-lang.org/download/ https://racket-lang.org/download/


# Install Dezhemini server

There two ways to install dezhemini, the Gemini server program. The easier one, is by using raco in Racket 8. The raco program is a command line utility and provides a package manager too. Using raco to install a package directly from a git repository is available from Racket version 8 or higher. Raco documentation Web page is [https://docs.racket-lang.org/raco/index.html]. 

The git repository for dezhemini is at [https://git.sr.ht/~rwv/dezhemini]. To install it just run the following command from any shell: 

```
raco pkg install git+https://git.sr.ht/~rwv/dezhemini
```

The output will show where it is installed. Usually, the program is installed at `~/.racket/8.3/bin/dezhmnsrv' if Racket version is 8.3. It is a good idea to add the bin directory to the current PATH. 


=> https://docs.racket-lang.org/raco/index.html https://docs.racket-lang.org/raco/index.html
=> https://git.sr.ht/~rwv/dezhemini https://git.sr.ht/~rwv/dezhemini


# Testing dezhemini server

There are several ways to test the Gemini server. It is required to use a TLS/SSL client first. Check [this StackExchange] question about how to connect to a TLS/SSL like a telnet. The first answer tells some utilities, one of the most relevant and available in all linux-like system is gnutls-cli. 


=> https://superuser.com/questions/346958/can-the-telnet-or-netcat-clients-communicate-over-ssl this StackExchange

## With gnutls-cli

The gnutls-cli program can connect to any SSL/TLS connection, and open the standard input to receive data from keyboard input and to send it to the server. It is very simmilar to telnet, but resolves the ciphered connection. 

For the Gemini protocol, some parameters are required: 

* --crlf :: To send CR LF characters when the enter key is pressed. 
* --insecure :: Ignore self-signed or insecure certificates. 
* -p 1965 :: Use port 1965. 

```Conectarse con gnutls-cli
gnutls-cli --crlf --insecure g.nubecita.online -p 1965
```

After the TLS connection information, enter the following text: 

```
gemini://g.nubecita.online/
```

Then, press enter to send the CR and LF characters. This will search for the index.gmi file and show it as output. 
