Ideas para Libreplanet 


# Flexibilidad del software libre ante la necesidad de herramientas

Demostrar que el software libre permite crear o modificar la herramientas. 

Ejemplos simples: ¿? ¡buscar! 

Ejemplos técnicos: 

* Caso de gemini-mode en emacs 
* Caso del servidor dezhemini 


# Para Libre Planet o EmacsConf

* Jabber.el 
* Ada (and Racket) uses Gemini 
* Ox-gemini 
* Literal Programming / Reproducible Research for Infosec people  
* Tunning the best Ruby enviroment for devs in EMACS 
* Manage Apis CVEs with EMACS 
* Formato de Reporte de vulnerabilidades con LaTeX en EMACS 
* Manejo de datos de Grafos de Conocimiento a traves de EMACS (Uso de triple stores!).
