# Listado de aplicaciones útiles para WM simples

Listado de aplicaciones simples para gestores de ventasas simples como I3WM y StumpWM. 

Muchos de estos programas se deben ejecutar en background. Por ejemplo: 

,----
| nohup xautolock -time 5 -locker 'i3lock -c 000000' &
`----


# Gestores de archivos

* pcmanfm 
* midnight commander (mc) 
* Emacs 
* ranger file manager 


# Íconos y Escritorio

Íconos en el escritorio. 

* idesk 


# Fondo de escritorio

Para cambiar el color o la imágen de fondo de escritorio: 

* feh 
* fbsetbg 
* xsetroot 


# Menús y launchers

Programas para mostrar menús de opciones. También se utilizan para lista aplicaciones y lanzarlas. 

* dmenu 
* rofi 
* dzen2 
* Emacs: `M-x counsel-linux-app' 
* zenity 

También, dzen se puede utilizar para mostrar documentación, mensajes y ayudas memoria. Tiene su propia sintaxis para color de texto, negrita, y otros formatos. 

zenity se puede utilizar para mostrar diálogos simples creados por parámetro. Por ejemplo: El siguiente comando solicita el nombre al usuario cambiando al español los botones y el título. 

,----
| zenity --entry --text="Ingrese su nombre" --entry-text="Juana" \
|        --title="Nombre" --ok-label="Aceptar" --cancel-label="Cancelar"
`----


# Configurar teclado

* setxkbmap -layout es -variant dvorak 
* loadkeys dvorak-es 


# Compositores

Transparencias y efectos gráficos. 

* xcompmgr 
* compton 
* picom 


# Clipboard

Para copiar texto usando la terminal: 

* xclip 


# Screen lockers

Protectores de pantallas: 

* i3lock 
* i3lock-fancy 
* xscreensaver 
* slock (suckless tools) 

,----
| xautolock -time 5 -locker 'i3lock -c 000000'
`----

Permite iniciar el protector de pantalla indicado al pasar cierta cantidad de tiempo de inactividad. 


# Estado del sistema

Aplicaciones para mostrar el estado del sistema. 


## Sobre el escritorio

* conky  


## Sobre la línea de estado

* i3status 
* i3pystatus 


## Barras de estado

* i3bar 


# Configuraciones

Iniciar temas para aplicaciones GTK/GTK+: /usr/libexec/mate-settings-daemon 


# Applets para la bandeja

Estas son aplicaciones que muestran un ícono o quedan en stand-by en la bandeja/tray. 


## Clipboard

* clipit 


## Network

* nm-applet 


## Sonido

* mate-volume-control-applet 


## Sincronización de archivos

* nextcloud 
* syncthing 

Nextcloud y syncthing son servicios completos, no solo muestran el ícono en la bandeja. 
