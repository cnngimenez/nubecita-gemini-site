#! /usr/bin/fish

source config/certificates.fish

if contains "$TLS_CLIENT_HASH" $my_certificates
    echo -e "20 text/gemini\r\n"

    echo "# ⤵ Descargando RSS desde nubecita.online..."    
    echo "=> /secure 🔙 Volver"
    echo

    echo "## Realizando `curl` en el sistema..."
    echo "```Salida de curl"
    echo "\$ curl -o \"$PWD/sitio/rss/main.xml\" 'https://nubecita.online/fudforum/feed.php?mode=m&l=1&basic=1'"
    curl -o "$PWD/sitio/rss/main.xml" 'https://nubecita.online/fudforum/feed.php?mode=m&l=1&basic=1'
    echo "```"

    echo

    echo "## Generando los gmi con gawk..."
    echo "```"

    set -l file "$PWD/sitio/rss/main.gmi"
    echo "# Feed principal" > "$file"
    echo -e "Se puede utilizar este gemtext como feed. Con Lagrange, haga clic derecho sobre la página y seleccione \"Suscribir\".\n\n" >> "$file"
    echo "\$ gawk -f \"$PWD/sitio/rss/rss2gmi.awk\" \"$PWD/sitio/rss/main.xml\" >> \"$file\""
    gawk -f "$PWD/sitio/rss/rss2gmi.awk" "$PWD/sitio/rss/main.xml" >> "$file"
    echo "```"
    
    echo "Listo."
else
    echo -e "61 Nubecita: Se requiere un certificado registrado en el servidor.\r\n"
end
