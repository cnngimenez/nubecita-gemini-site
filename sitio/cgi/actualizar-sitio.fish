#! /usr/bin/fish
source config/certificates.fish

if contains "$TLS_CLIENT_HASH" $my_certificates
    echo -e "20 text/gemini\r\n"

    echo "# ⤵ Git Pull..."
    echo "Se está realizando `git pull` en el sistema."
    echo "=> /secure 🔙 Volver"
    echo
    echo "Salida:"
    echo "```Salida de git pull"
    echo "\$ git pull"
    git pull
    echo "```"
else
    printf "61 Nubecita: Se requiere un certificado registrado en el servidor.\r\n"
end
