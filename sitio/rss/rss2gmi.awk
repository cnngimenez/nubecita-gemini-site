#! /bin/gawk -f

BEGIN {
  start_item = 0
  start_description = 0
  title = ""
  link = ""
  date = ""
  desc = ""
  creator = ""
}

/<item/ {
  start_item = 1
}

/<title>/ {
  match($0, /<title>(.*)<\/title>/, arr)
  title = arr[1]
  next
}

/<link>/ {
  match($0, /<link>(.*)<\/link>/, arr)
  link = arr[1]
  next
}

/<dc:date>/ {
  match($0, /<dc:date>(.*)T.*<\/dc:date>/, arr)
  date = arr[1]
  next
}

/<dc:creator>/ {
  match ($0, /<dc:creator>(.*)<\/dc:creator>/, arr)
  creator = arr[1]
  next
}

/<description>/ {
  start_description = 1
  match($0, /<description>(.*)(<\/description>)?/, arr)
  desc = "```Descripción"
  desc = desc "\n" arr[1]
  sub(/<\/description>/, "", desc)
  sub(/<!\[CDATA\[/, "", desc)
  if (match(arr[0], /<\/description>/) > 0){
    start_description = 0
    sub(/\]\]>/, "", desc)
    desc = desc "\n```"
  }
  next
}
/<\/description>/{
  if (start_description == 1){
    start_description = 0
    match($0, /(.*)(]]>)?<\/description>/, arr)
    sub(/\]\]>/, "", arr[1])    
    desc = desc "\n" arr[1] "\n```"
    next
  }
}

/<\/item>/ {
  if (start_item == 1) {
    print "=> " link " " date " - " date " - " title " 👤" creator
    print desc
    next;
  }
}

/./ {
  if (start_description == 1){
    match($0, /(.*)(]]>)?/, arr)
    desc = desc "\n" arr[1]
    next
  }
}
