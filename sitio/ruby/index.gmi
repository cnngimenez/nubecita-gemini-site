# Sección dedicada a Ruby

=> ./opp.gmi OPP

=> https://www.theodinproject.com/lessons/ruby-caesar-cipher The Odin Project

## [The Odin Project]

### Project: Caesar Cipher

* Assignment

Implement a caesar cipher that takes in a string and the shift factor and then outputs the modified string: 
```
 > caesar_cipher("What a string!", 5)
 => "Bmfy f xywnsl!"
```

* Quick Tips: 

* You will need to remember how to convert a string into a number. 
* Don’t forget to wrap from z to a. 
* Don’t forget to keep the same case. 


## Posible solución 1

```

$alfabeto_mayusculas = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
$alfabeto_minusculas = "abcdefghijklmnopqrstuvwxyz"
$longitud_alfabeto = 26
$limite_inferior_mayusculas_ascii = 65
$limite_inferior_minusculas_ascii = 97

def cesar(cadena, clave, orientacion = 1)
  cadena_modificada = ""
  for letra in cadena.chars #para cada letra del string
    if !letra.match(/^[[:alpha:]]$/) # si es un caracter
     cadena_modificada += letra #la sumo como está
     next
    elsif letra==letra.upcase #si es mayusculas
      alfabeto = $alfabeto_mayusculas #ABCDEFGHIJKLMNOPQRSTUVWXYZ
      limite = $limite_inferior_mayusculas #65 al 90
    elsif letra == letra.downcase  #si es minuscula
      alfabeto = $alfabeto_minusculas #abcdefghijklmnopqrstuvwxyz
      limite = $limite_inferior_minusculas #97 al 122
    end
    valor_ascii = letra.ord #le asigno su valor en ascii
    direccion = clave * orientacion #defino si sumo o resto, izquierda o derecha, positivo negativo
    nueva_posicion = (valor_ascii - limite + direccion) % $longitud_alfabeto #posicion en el alfabeto
    cadena_modificada += alfabeto[nueva_posicion] #cadena = cadena + Alfabeto[posicion]
  end
      return cadena_modificada
end

```
## Posible Solución 2*

```
def cesar(frase, clave)
alfabeto=("a".."z")
frase=frase.chars
cifrado=""
frase.each do |letra|
  if alfabeto.include?(letra.downcase) #si la letra esta en el alfabeto
  clave.times {letra = letra.next} #me muevo tantas veces en el abecedario como diga la clave
  elsif alfabeto.include?(letra.upcase)
  clave.times{letra=letra.next}
end
cifrado += letra
end
    cifrado
end

print "Escribe el mensaje que quiere cifrar: "
frase= gets.to_str
print "Escribre la clave que quieres utilizar: "
clave= gets.to_i
puts  "El mensaje cifrado es: #{cesar(frase, clave)}"


```
*\ solo cifrado, tomando en cuenta mayúsculas y minúsculas, no caracteres especiales ni numéricos
## Posible Solución 3:
```
A=("A".."Z").to_a
a=("a".."z").to_a

C=A.concat(a)
def cesar (frase, key)
 n=[]
 arreglo=frase.chars
 #iteramos sobre la frase
 arreglo.each do |i| #para cada letra
 if  C.include?(i) #si esta incluida en el arreglo
 n = n.push(i.ord + key)#la agrego a un nuevo arreglo en ascii
 end
 end
 m=[] #creo un nuevo arreglo
 n.each do |x| #recorro el arreglo en ascii creado
 m=m.push(x.chr) #agrego elementos no ascii
 end
 m.join #convierto el arreglo en un string
end

puts "Escribe el mensaje que quiere cifrar o descrifrar: "
frase= gets.to_str
puts "Escribe la clave que quieres utilizar (nro negativo para descifrar): "
key= gets.to_i
puts  "El mensaje cifrado es: #{cesar(frase, key)}"

```
## Posible Solución 4:

```
=begin
('!'..'~').to_a
# => [
#     "`", "!", "\"", "#", "$", "%", "&", "'", "(", ")", "*", "+", ",",
#     "-", ".", "/", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
#     ":", ";", "<", "=", ">", "?", "@", "A", "B", "C", "D", "E", "F",
#     "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S",
#     "T", "U", "V", "W", "X", "Y", "Z", "[", "\\", "]", "^", "_", "a",
#     "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
#     "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "{",
#     "|", "}", "~"
# ]
=end
ASCII=("!".."~").to_a
def cesar (frase, key)
  arreglo=frase.chars
 arreglo.each do |i|
 n=[]
 if  ASCII.include?(i)
 n = n.push(i.ord + key)
 end
 end
 m= n.pack('c*')
end

puts "Escribe el mensaje que quiere cifrar o descrifrar: "
frase= gets.to_str
puts "Escribe la clave que quieres utilizar (nro negativo para descifrar): "
key= gets.to_i
puts  "El mensaje cifrado es: #{cesar(frase, key)}"
```

* TODO: Próxima solución mediante la utilización de clases

## Solución

## Se podrá hacer algún proyecto similar para 'EMACS NEWS' en Ruby?

=> https://www.ayush.nz/2018/09/tweet-toot-building-a-bot-for-mastodon-using-python Tweet-Toot

. Ya tenemos cuenta creada:
=> https://mastodon.social/@emacsbot Cuenta Mastodon Bot EMACS

. Referencias:
=> https://sachachua.com/blog/category/emacs-news/ EMACS NEWS

#Otros Recursos

## Cliente Gemini
=> https://git.umaneti.net/ruby-net-text/

## Mastodon API
=> https://rubygems.org/gems/mastodon-api

## Paquetes EMACS
. Yari
. Rubocop
. Inf-ruby
. Flycheck

## Comandos EMACS para trabajar con Ruby
-M-x run-ruby
-M-x yari
-M-x flycheck-mode
-M-x flycheck-list-errors
-M-x rubocop-mode
-El famoso C-c C-l para cargar el buffer en Ruby.
-C-c M-r (ruby-send-region-and-go)
-C-x C-e (ruby-send-last-stmt) (¡similar al de Elisp!)
-C-c C-b (ruby-send-block)
-Y otros M-x ruby-send-ALGO...
## Importante
- Los nombres de clase siempre empiezan con mayúscula (nombres compuestos, camel case)
- Constantes: todo en mayúsculas: EULER, PI, ALFABETO
- Variables globales: $algo
- Variables de instancia: @algo
- Varibales de clase: @@algo
- Variables locales: algo
- clases: Cesar
- módulos: Cifrados
- métodos: cifrar
- métodos de clase: Cesar::cifrar (hay otra manera)
- métodos de tipo perdicados: MENSAJE?, ejemplo: "oetnurneo".es_cesar?
- métodos que no generan copias (modifican el objeto actual): "hola".cortar_string! 2
- métodos que generan copias (no modifican el objeto actual): "hola".cortar_string 2
- pasaje de métodos como argumento: &:MENSAJE, por ejemplo: "230145".each &:digit?
